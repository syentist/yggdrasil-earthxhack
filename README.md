# YDDRASIL #

YGGDRASIL (Your Global Guide to Dynamic Research & Analysis / Scientific Interactive Library) is a 3D world tree map 
consisting of informatics overlays that display visualizations of statistical ecological data in real time.